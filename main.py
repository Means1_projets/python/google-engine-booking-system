import os
from flask import Flask, render_template, redirect, url_for, request
from utils import *
from form import *

firebase_request_adapter = requests.Request()

datastore_client = datastore.Client()

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


#def store_default_models(user):
#    entity_room = datastore.Entity(key=datastore_client.key('room', 'Room 1'))
#    entity_room.update({
#        'user': user.key.name,
#        'size': 42
#    })
#    entity_booking = datastore.Entity(key=datastore_client.key('booking'))
#    entity_booking.update({
#        'user': user.key.name,
#        'room': entity_room.key.name,
#        'date_start': datetime.now(),
#        'date_end': datetime.now(),
#    })
#    datastore_client.put(entity_room)
#    datastore_client.put(entity_booking)


@app.route('/', methods=["GET", "POST"])
def root():
    return root_with_form(AddRoom(), DeleteRoom())


def root_with_form(form_add_room, roomdelete):
    claims, error_message = auth(request.cookies.get("token"))
    rooms = []
    hasbooking = False
    bookings = []
    booksearch = Search()
    deleteeditbooking = DeleteOrEditBooking()
    if claims is not None:
        user = fetch_user(claims)
        rooms = fetch_all_room()

        choices = list(map(lambda p: (p.key.name, p.key.name), rooms))
        choices.append(("all", "all"))
        booksearch.name_room.choices = choices
        if booksearch.validate_on_submit():
            if booksearch.name_room.data == "all":
                if booksearch.ismine.data:
                    bookings = find_all_booking_for_a_user(claims["user_id"])
                else:
                    bookings = find_all_booking()
            else:
                bookings = find_all_booking_for_a_room(booksearch.name_room.data)
            if booksearch.ismine.data:
                bookings = list(filter(lambda x: x["user"] == claims["user_id"], bookings))
            if booksearch.date_start.data is not None:
                bookings = list(filter(lambda x: x["date_end"] > utc.localize(booksearch.date_start.data), bookings))
            if booksearch.date_end.data is not None:
                bookings = list(filter(lambda x: x["date_start"] < utc.localize(booksearch.date_end.data), bookings))
        else:
            bookings = find_all_booking_for_a_user(claims["user_id"])
            booksearch.name_room.data = "all"
            booksearch.ismine.data = True
        hasbooking = len(bookings) > 0
    return render_template(
        'index.html',
        user_data=claims, error_message=error_message,
        form_add_room=form_add_room, rooms=rooms,
        hasbooking=hasbooking, bookings=bookings, roomdelete=roomdelete,
        deleteeditbooking=deleteeditbooking, booksearch=booksearch)

@app.route("/addroom", methods=["POST"])
def addroom():
    claims, error_message = auth(request.cookies.get("token"))
    form_add_room = AddRoom()
    if claims is not None and form_add_room.validate_on_submit():
        addroom_database(form_add_room, claims["user_id"])
        return redirect(url_for('root'))
    return root_with_form(form_add_room, DeleteRoom())


@app.route("/booking/<room>")
def booking(room):
    return booking_with_form(room, AddBooking())

def booking_with_form(room, form_add_booking):
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    room = find_by_name(room)
    bookings = find_all_booking_for_a_room(room.key.name)
    bookings = sorted(bookings, key=lambda x: x["date_start"])
    hasbooking = len(bookings) > 0
    deleteeditbooking = DeleteOrEditBooking()
    form_add_booking.room.data = room.key.name
    roomdelete = DeleteRoom()
    roomdelete.room.data = room.key.name
    return render_template(
        'room.html',
        user_data=claims, error_message=error_message, room=room, form_add_booking=form_add_booking,
        bookings=bookings, hasbooking=hasbooking, deleteeditbooking=deleteeditbooking, roomdelete=roomdelete)

@app.route("/add_booking", methods=["POST"])
def add_booking():
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    add_booking = AddBooking()
    room = find_by_name(add_booking.room.data)
    if room is None:
        return redirect(url_for('root'))
    if add_booking.validate_on_submit():
        save_booking(room.key.name, add_booking, claims["user_id"])
        return redirect(url_for('root'))
    else:
        return booking_with_form(room.key.name, add_booking)

@app.route("/deletebooking", methods=["POST"])
def deletebooking():
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    deletebooking = DeleteOrEditBooking()
    if deletebooking.validate_on_submit():
        booking = find_booking_by_id(deletebooking.booking_id.data)
        if booking is not None and booking["user"] == claims["user_id"]:
            datastore_client.delete(booking.key)
        return redirect(url_for('root'))
    return redirect(url_for('root'))


@app.route("/editbooking", methods=["POST"])
def editbooking():
    return editbooking_with_form(EditBooking())

def editbooking_with_form(edit_booking):
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    deletebooking = DeleteOrEditBooking()
    if deletebooking.validate_on_submit():
        booking = find_booking_by_id(deletebooking.booking_id.data)
        if booking is not None and booking["user"] == claims["user_id"]:
            room = find_by_name(booking["room"])
            bookings = find_all_booking_for_a_room(room.key.name)
            bookings = sorted(bookings, key=lambda x: x["date_start"])
            edit_booking.date_start.data = booking["date_start"]
            edit_booking.date_end.data = booking["date_end"]
            edit_booking.booking_id.data = booking.key.id
            edit_booking.room.data = room.key.name
            return render_template(
                'edit.html',
                user_data=claims, error_message=error_message, room=room,
                edit_booking=edit_booking, bookings=bookings)
    return redirect(url_for('root'))

@app.route("/post_edit_booking", methods=["POST"])
def post_edit_booking():
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    editbooking = EditBooking()
    room = find_by_name(editbooking.room.data)
    if room is None:
        return redirect(url_for('root'))
    if editbooking.validate_on_submit():
        booking = find_booking_by_id(editbooking.booking_id.data)
        if booking is not None and booking["user"] == claims["user_id"]:
            updatebooking(booking, editbooking)
        return redirect(url_for('root'))
    else:
        return editbooking_with_form(editbooking)

@app.route("/deleteroom", methods=["POST"])
def deleteroom():
    claims, error_message = auth(request.cookies.get("token"))
    if claims is None:
        return redirect(url_for('root'))
    deleteroom = DeleteRoom()
    if deleteroom.validate_on_submit():
        room = find_by_name(deleteroom.room.data)
        if room is not None and room["user"] == claims["user_id"]:
            datastore_client.delete(room.key)
    return root_with_form(AddRoom(), deleteroom)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
