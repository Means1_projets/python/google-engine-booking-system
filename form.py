from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, DateTimeField, BooleanField, SelectField
from wtforms.validators import DataRequired, ValidationError, Optional
from google.cloud import datastore
from datetime import *
from utils import *
import pytz

utc=pytz.UTC

datastore_client = datastore.Client()


def validate_name_exist(form, field):
    query = datastore_client.query(kind="room")
    if len(list(query.add_filter('__key__', "=", datastore_client.key("room", field.data)).fetch())) > 0:
        raise ValidationError("This name is already is the database")

def removeillegalCaractere(form, field):
    if "/" in field.data:
        raise ValidationError("You cant use the /")

def size_sup_0(form, field):
    if field.data is not None and field.data <= 0:
        raise ValidationError("The size need to be positive")

def check_if_date_is_invalide(form, field):
    if field.data is None:
        raise ValidationError("You need to set a date !")
    if field.data < datetime.now():
        raise ValidationError("The date is invalid")

def check_valide_range(form, field):
    if form.date_start.data is None or form.date_end.data is None:
        return
    room = find_by_name(form.room.data)
    if room is None:
        raise ValidationError("Error room !")
    booking = find_all_booking_for_a_room(room.key.name)

    date_start = utc.localize(form.date_start.data)
    date_end = utc.localize(form.date_end.data)

    if date_end < date_start:
        raise ValidationError("The range is impossible")

    if type(form) == EditBooking:
        booking = filter(lambda x: x.key.id != form.booking_id.data, booking)
    dates = list(map(lambda x: (x["date_start"], x["date_end"]), booking))
    dates = list(filter(lambda x: date_start <= x[1], dates))
    dates = list(filter(lambda x: date_end >= x[0], dates))

    if len(dates) > 0:
        raise ValidationError("There is already a booking in this range")

class AddRoom(FlaskForm):
    name = StringField('name', validators=[validate_name_exist, removeillegalCaractere])
    desc = StringField('desc')
    size = IntegerField('size', validators=[size_sup_0])

class Search(FlaskForm):
    name_room = SelectField("name")
    ismine = BooleanField("My booking")
    date_start = DateTimeField("date_start", format='%Y-%m-%dT%H:%M', validators=[Optional()])
    date_end = DateTimeField("date_end", format='%Y-%m-%dT%H:%M', validators=[Optional()])


class AddBooking(FlaskForm):
    room = StringField("room")
    date_start = DateTimeField("date_start", format='%Y-%m-%dT%H:%M', validators=[check_if_date_is_invalide])
    date_end = DateTimeField("date_end", format='%Y-%m-%dT%H:%M', validators=[check_if_date_is_invalide, check_valide_range])

class DeleteOrEditBooking(FlaskForm):
    booking_id = IntegerField("booking_id")

class EditBooking(AddBooking):
    booking_id = IntegerField("booking_id")


def check_if_user_can_delete(form, field):
    room = find_by_name(form.room.data)
    if room is None:
        raise ValidationError("Error room !")
    booking = find_all_booking_for_a_room(room.key.name)
    dates = list(map(lambda x: (x["date_start"], x["date_end"]), booking))
    dates = list(filter(lambda x: utc.localize(datetime.now()) < x[1], dates))
    print(dates)
    if len(dates) > 0:
        raise ValidationError("You cant delete this room, there are booking still coming soon")


class DeleteRoom(FlaskForm):
    room = StringField("room", validators=[check_if_user_can_delete])