window.addEventListener('load', function () {
  document.getElementById('sign-out').onclick = function () {
    firebase.auth().signOut();
    setTimeout(function(){ location.reload(); }, 500);
  };

  // FirebaseUI config.
  var uiConfig = {
    signInSuccessUrl : "/",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    // Terms of service url.
    tosUrl: 'localhost'
  };

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in, so display the "sign out" button and login info.
      document.getElementById('sign-out').hidden = false;
      document.getElementById('login-info').hidden = false;
      console.log(`Signed in as ${user.displayName} (${user.email})`);
      user.getIdToken().then(function (token) {

        document.cookie = "token="+token+ "; path=/"

      });
    } else {
      // User is signed out.
      // Initialize the FirebaseUI Widget using Firebase.
      var ui = new firebaseui.auth.AuthUI(firebase.auth());
      // Show the Firebase login button.
      ui.start('#firebaseui-auth-container', uiConfig);
      // Update the login state indicators.
      document.getElementById('sign-out').hidden = true;
      document.getElementById('login-info').hidden = true;
      // Clear the token cookie.
      document.cookie = "token=; path=/"
    }
  }, function (error) {
    console.log(error);
    alert('Unable to log in: ' + error)
  });

  var deleteroomModal = document.getElementById('deleteModal')
  if (deleteroomModal !== null)
    deleteroomModal.addEventListener('show.bs.modal', function (event) {
      // Button that triggered the modal
      var button = event.relatedTarget
      // Extract info from data-bs-* attributes
      var recipient = button.getAttribute('data-bs-room')
      // If necessary, you could initiate an AJAX request here
      // and then do the updating in a callback.
      //
      // Update the modal's content.
      var modalBodyInput = deleteroomModal.querySelector('.modal-body #room')
      modalBodyInput.value = recipient
    })

  var deletebookingModal = document.getElementById('deleteBookingModal')
  if (deletebookingModal !== null)
    deletebookingModal.addEventListener('show.bs.modal', function (event) {
      // Button that triggered the modal
      var button = event.relatedTarget
      // Extract info from data-bs-* attributes
      var recipient = button.getAttribute('data-bs-booking')
      // If necessary, you could initiate an AJAX request here
      // and then do the updating in a callback.
      //
      // Update the modal's content.
      var modalBodyInput = deletebookingModal.querySelector('.modal-body #booking_id')
      modalBodyInput.value = recipient
    })

});
