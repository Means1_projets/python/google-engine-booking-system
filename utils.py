from google.auth.transport import requests
import google.oauth2.id_token
from google.cloud import datastore

firebase_request_adapter = requests.Request()
datastore_client = datastore.Client()

def auth(id_token):
    if id_token:
        try:
            return google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter), None
        except ValueError as exc:
            print(exc)
            return None, str(exc)
    return [None, None]

def fetch_all_room():
    query = datastore_client.query(kind='room')
    return list(query.fetch())

def fetch_user(user_claim):
    key = datastore_client.key('user', user_claim["user_id"])
    query = datastore_client.query(kind='user')
    query.add_filter("__key__", "=", key)
    users = list(query.fetch())
    if len(users) > 0:
        return users[0]
    entity = datastore.Entity(key=key)
    entity.update({
        "name": user_claim["name"]
    })
    datastore_client.put(entity)
    return entity

def addroom_database(form_add_room, user_id):
    entity = datastore.Entity(key=datastore_client.key('room', form_add_room.name.data))
    entity.update({
        'size': form_add_room.size.data,
        'desc': form_add_room.desc.data,
        'user': user_id
    })
    datastore_client.put(entity)

def find_by_name(room_name):
    key = datastore_client.key('room', room_name)
    query = datastore_client.query(kind='room')
    query.add_filter("__key__", "=", key)
    rooms = list(query.fetch())
    if len(rooms) > 0:
        return rooms[0]
    return None

def find_all_booking_for_a_room(room_name):
    query = datastore_client.query(kind='booking')
    query.add_filter("room", "=", room_name)
    bookings = list(query.fetch())
    return bookings


def find_all_booking_for_a_user(user_name):
    query = datastore_client.query(kind='booking')
    query.add_filter("user", "=", user_name)
    bookings = list(query.fetch())
    return bookings


def save_booking(room_name, add_booking, user_id):
    entity = datastore.Entity(key=datastore_client.key('booking'))
    entity.update({
        'room': room_name,
        'user': user_id,
        'date_start': add_booking.date_start.data,
        'date_end': add_booking.date_end.data,
    })
    datastore_client.put(entity)

def find_booking_by_id(id):
    key = datastore_client.key('booking', id)
    query = datastore_client.query(kind='booking')
    query.add_filter("__key__", "=", key)
    bookings = list(query.fetch())
    if len(bookings) > 0:
        return bookings[0]
    return None

def updatebooking(entity, booking):
    entity.update({
        'date_start': booking.date_start.data,
        'date_end': booking.date_end.data,
    })
    datastore_client.put(entity)


def find_all_booking():
    query = datastore_client.query(kind='booking')
    bookings = list(query.fetch())
    return bookings